add_definitions(-DKSMSERVER_UNIT_TEST)
include(ECMMarkAsTest)

include_directories(${CMAKE_CURRENT_BINARY_DIR}/..)
set( kcmTest_SRCS
     kcmtest.cpp
     ../kcm.cpp
     ../lookandfeeldata.cpp
     ../../kcms-common_p.h
)

if (X11_Xcursor_FOUND)
    set(kcmTest_SRCS
        ${kcmTest_SRCS}
        ../../cursortheme/xcursor/cursortheme.cpp
        ../../cursortheme/xcursor/xcursortheme.cpp
    )
endif ()

kconfig_add_kcfg_files(kcmTest_SRCS ../lookandfeelsettings.kcfgc GENERATE_MOC)

add_executable(kcmTest ${kcmTest_SRCS})

add_dependencies(kcmTest kcm_lookandfeel)

target_link_libraries(kcmTest
        Qt::DBus
        Qt::Test
        KF5::I18n
        KF5::KCMUtils
        KF5::IconThemes
        KF5::JobWidgets
        KF5::Service
        KF5::QuickAddons
        KF5::KIOWidgets
        KF5::Declarative
        PW::KWorkspace
        X11::X11
        krdb
)

if(X11_FOUND)
    target_link_libraries(kcmTest Qt::X11Extras)
endif()
if (X11_Xcursor_FOUND)
   target_link_libraries(kcmTest X11::Xcursor)
endif ()
if (X11_Xfixes_FOUND)
   target_link_libraries(kcmTest X11::Xfixes)
endif ()

add_test(NAME lookandfeel-kcmTest COMMAND kcmTest)
ecm_mark_as_test(kcmTest)

